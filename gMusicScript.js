setTimeout(initialSetup, 500);
let currentSong = "";
let globaThread = 0;

function initialSetup() {
    try {
        document.getElementById("player-bar-forward").onclick = songSkipped;
        document.querySelector('[title="Thumb-down"]').onclick = songSkipped;
        setTimeoutInThread(checkDislikedSong, 0, 500);
    } catch(err) {
        console.error(err);
        console.log("music not running will trey in 2 seconds");
        setTimeout(initialSetup, 2000);
    }
}

function checkDislikedSong(threadNum) {
    if (threadNum != globaThread) {
        console.log("not in current thread, current = " + globaThread + " instead " + threadNum);
        return;
    }

    let actualSong = document.getElementById("currently-playing-title").title;

    if (currentSong != actualSong) {
        currentSong = actualSong;
        if (document.querySelector('[title="Undo thumb-down"]')){
            nextSong();
            return;
        } else {
            console.log("normalsong")
        }
    }
    let nextRequestIn = getTimeLeft();
    console.log("time left " + nextRequestIn);
    setTimeoutInThread(checkDislikedSong, threadNum, (nextRequestIn + 1) * 1000);
}

function songSkipped() {
    globaThread = globaThread + 1;
    setTimeoutInThread(checkDislikedSong, globaThread, 500);
}

function setTimeoutInThread(callback, threadNum, timeout) {
   setTimeout(function() {callback(threadNum);}, timeout);
}

function nextSong() {
    console.log("ok, next");
    document.getElementById("player-bar-forward").click();
}

/**
 * @returns {number} of seconds for this song to end
 */
function getTimeLeft() {
    let currentTime = stringTimeToSecondsValue(document.getElementById("time_container_current").innerHTML);
    let totalTime = stringTimeToSecondsValue(document.getElementById("time_container_duration").innerHTML);
    return totalTime - currentTime;
}

/**
 *
 * @param time in format as 3:21 or 0:12 or 11:16 etc.
 * @return time in seconds as integer value
 */
function stringTimeToSecondsValue(time) {
    let minSecondsArray = time.split(":");
    let minutes = parseInt(minSecondsArray[0]);
    let seconds = parseInt(minSecondsArray[1]);
    return minutes * 60 + seconds;
}

//if ($("paper-icon-button[title='Undo thumb-down']")) {console.log("thumdowned")} else {console.log("normalsong")}
// $('#item1 span').html();
//$("#time_container_duration").innerHTML
//$("#time_container_current").innerHTML