// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be

var queryInfo = {
  url: "https://play.google.com/music*"
};

function gMusicTab(callback) {
    chrome.tabs.query(queryInfo, (tabs) => {
            for (let i = 0; i < tabs.length; i++) {
                callback(tabs[i]);
            }
});}

document.addEventListener('DOMContentLoaded', () => {
    let skipCheckboxIsOn = document.querySelector("input[id=skipCheckbox]");

    document.getElementById("mainTitle").innerHTML = chrome.i18n.getMessage("mainTitle");
    document.getElementById("checkboxLabel").innerHTML = chrome.i18n.getMessage("checkboxLabel");

    skipCheckboxIsOn.addEventListener( 'change', function() {
        if(this.checked) {
            console.log("lets roll");
            gMusicTab(function(tab) {
                chrome.tabs.executeScript(tab.id, {file: "gMusicScript.js"});
            });
        } else {
            console.log("no action defined");
            gMusicTab(function(tab) {
                chrome.tabs.executeScript(tab.id, {file: "empty.js"});
            });
        }
    });


});



